\documentclass{scrartcl}
\usepackage[italian]{babel}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage[stable]{footmisc}
\usepackage{parskip}
\usepackage[mark]{gitinfo2}
\usepackage[leftbars]{changebar}

\newcommand{\versiontag}{ver.\gitAbbrevHash\ \ \gitAuthorDate}

\title{Divertimento\footnotemark[0]\\[-0.5\baselineskip]{\tiny (\versiontag)}}

\author{Nicola Bernardini}
\date{23 maggio 2020}

\newcommand{\imagedir}{../images}

\setlength{\parskip}{1.2\baselineskip}
\setlength{\parindent}{0pt}

\begin{document}

\maketitle
\footnotetext{\hspace{1mm}
\includegraphics[width=0.06\textwidth]{\imagedir/cc-by-sa}
\emph{Divertimento} (2020) is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
Based on a work at \url{https://gitlab.com/nicb/Divertimento}.
}

\section{Divertimento - un'introduzione}

L'etimo del verbo \emph{divertire} \`e legato al latino
\emph{divertere}. Altre parole significativamente collegate a questo verbo
sono \emph{diverso} e \emph{divorzio}. Esso \`e composto dalla particella \emph{DI(S)}
che significa allontanamento e \emph{VERTERE}, oss\`ia volgere.
Quindi? \emph{volgere altrove}, far prendere altra direzione.

Il dizionario etimologico ci informa ovviamente anche del significato figurato
di questa parola: "distogliere, [\ldots] ricreare, sollazzare, distraendo l'animo da
cure e pensieri molesti".

L'idea che un \emph{divertimento} non sia semplice svago ma piuttosto un
improvviso cambiamento di direzione, un dirottamento e infine una
\emph{diversione} (appunto) non \`e mia: nelle lunghe chiaccherate con Luciano
Berio era emerso questo significato pi\`u profondo\footnote{
In queste discussioni si parlava dei molti \emph{Divertimenti} 
da lui composti negli anni '50, spesso in compagnia e in complicit\`a
con Bruno Maderna, in seno al loro lavoro di programmisti RAI.
} che mi accompagna ancora oggi.
Col passare del tempo ho capito che non solo il divertimento \`e un momento di apertura
ad altro, all'inedito e inaspettato, ma che \`e anche una componente fondante
del senso delle nostre vite -- il quale tende facilmente a perdersi nella
stupida macina quotidiana dei problemi, dei doveri e delle aspettative di
un'esistenza ormai irrimediabilmente alienata.

Il problema \`e che non sappiamo pi\`u divertirci.

La nostra esistenza di individui \emph{per decreto} teorizzata da Zygmunt Bauman,
oltre ad aver alienato il senso di elementi fondanti quali le passioni e il desiderio, 
ci ha privato della capacit\`a (totalmente soggettiva e individuale)
di \emph{divertirci}, ossia di volgere lo sguardo altrove,
di raggiungere quel ``altro da s\'e'' che, solo, \`e in grado di restituire
senso alle traiettorie di vita che, ironicamente, si allungano sempre di pi\`u
- lasciando spazio a seconde e financo a terze vite.
Se aggiungiamo in questa miscela l'evoluzione delle societ\`a odierne in
societ\`a di servizi con conseguente emergenza di un ceto terziario
avanzato al quale vengono regolarmente affidate mansioni burocratiche,
ripetitive e per nulla gratificanti abbiamo un quadro pi\`u o meno completo (e
terrificante) della situazione.
La vita \`e diventata irrimediabilmente lunga e noiosa
e il dilagare delle patologie depressive e delle dipendenze da agenti tossici
come cibo, fumo, alcool e droghe di tutte le qualit\`a
\`e un segnale esorbitante che facciamo fatica a ignorare.

Naturalmente, tutte le societ\`a civili sono corse ai ripari.
Anzi: \`e nata una vera e propria industria del divertimento con proventi da capogiro.
Veniamo tutti imbottiti di svaghi di massa -- gli unici con i quali si possano
massimizzare i profitti -- e la societ\`a dell'informazione si \`e
perfettamente inserita in questo ecosistema rendendo onanistico (e quindi
endogeno) il \emph{divertimento}. I \emph{social networks}, la televisione
\emph{on demand}, la musica \emph{on tap}, le notizie in tempo reale -- sono
tutti tasselli di un gigante mosaico che martella le nostre vite quotidiane
per \emph{divertirci} in ogni momento. La verit\`a \`e che si tratta di
divertimenti posticci, fatui, privi di consistenza semantica -- sono in
realt\`a \emph{distrazioni} pi\`u che divertimento. Ed ecco che compaiono gli
studi sulle dipendenze da \emph{smartphone}, da videogiochi, da pornografia,
ecc. E, di converso, ecco i libri su come \emph{essere migliori}, come \emph{essere
felici}, come \emph{vivere intensamente la propria vita}, ecc.

Il fatto \`e che il \emph{divertimento} \`e faticoso.

Per divertirsi davvero, per avere delle epifanie di pensiero che facciano
crescere la consapevolezza di mondo che ci circonda, occorre studiare.
Pi\`u studiamo, pi\`u approfondiamo i nostri oggetti di desiderio, pi\`u
elaboriamo un pensiero critico in grado di analizzarli, e pi\`u ci
\emph{divertiamo} con essi - e questa volta si tratta di \emph{divertimenti}
importanti, che riempiono di senso le nostre vite dandoci qualcosa al quale
aggrapparci per non essere trascinati via dai flutti del quotidiano.

In verit\`a, ci sono alcuni divertimenti di massa che godono di pubblici
estremamente vasti nonostante siano assai faticosi.
Vorrei delinearne qui due:
la \emph{fitness} e il \emph{turismo}.
Entrambi pretendono dai propri praticanti sacrifici e fatica,
e forse per questo riescono a dare pi\`u senso di altri divertimenti.
Come funzionano questi divertimenti?

La \emph{fitness} produce dei risultati visibili sul benessere e sull'aspetto
fisico in un tempo relativamente breve, e si tratta di risultati estremamente
gratificati dalle societ\`a di oggi: l'aderenza pi\`u rigorosa ai canoni
vigenti di bellezza fisica unita al generale stato di salute dell'individuo
forniscono incentivi pi\`u che sufficienti ad affrontare sacrifici e fatica.
Prova ne sia la quantit\`a di palestre che sono comparse a qualsiasi
latitudine del globo: la \emph{fitness} sembra essere un linguaggio universale
delle societ\`a contemporanee. Come in tutte le produzioni di consumo,
l'offerta si \`e diversificata all'inverosimile, creando prodotti per
tutti i palati, dalla \emph{ginnastica dolce} al \emph{jogging}, dal \emph{pilates}
al \emph{cross--fit} al \emph{body building}. E -- come giustamente rilevava
Bauman -- mentre la salute \`e una qualit\`a binaria (\`e presente o
mancante), non ci sono limiti alla quantit\`a di \emph{fitness}.
Essa non \`e mai abbastanza: una vera gallina dalle uova d'oro per
un mondo consumista come il nostro.

Il turismo \`e un divertimento costoso che ha spesso bisogno di sacrifici
consistenti da parte degli appassionati, senza dimenticare che viaggiare \`e
un'attivit\`a faticosa e talvolta tediosa. Nonostante questo, il turismo \`e
praticato da folle oceaniche di persone che si spostano tutti gli anni
in ogni parte del globo - lo fanno comunque, anche se per il resto della
propria vita abitano in luoghi meravigliosi (a loro volta visitati da
\emph{altre} folle oceaniche). Cos'\`e che alimenta questo \emph{divertimento
faticoso}? I motivi sono evidenti quanto ovvi: la vita alienata di oggi non
ci consente di accumulare avvenimenti sostanziali, eclatanti, che si
sedimentino prima nel nostro immaginario e poi nella nostra memoria.
Abbiamo tutti bisogno quindi di questi surrogati di vissuto che possano
costituire una possibile semantica di vita: trascorriamo la maggior parte
di quest'ultima senza avere assolutamente nulla da raccontare di una
quotidianit\`a devastante, ma i nostri giri turistici colmano la vacuit\`a
di fotografie e di ricordi pi\`u o meno superficiali.
Anche qui, ce n'\`e per tutti i gusti: dalle \emph{vacanze--avventura} alle
mete esotiche, dalle citt\`a d'arte alle antichit\`a alla natura (pseudo--)selvaggia.
Ce n'\`e per tutti i gusti, ed \`e virtualmente impossibile visitare/vivere
tutto in una sola vita, per quanto questa si allunghi e il turismo diventi
\emph{mordi--e--fuggi}. Non ci meraviglieremo quindi del successo di massa di
questo divertimento.

\section{La ginnastica del pensiero}

Cosa manca in maniera eclatante in questa minuscola disamina dei \emph{divertimenti faticosi}?

Manca il \emph{pensiero}.

Pensare \`e ritenuto dai pi\`u un'attivit\`a non
solo faticosa, ma anche i cui sforzi somigliano troppo a quelli delle ``cure e
dei pensieri molesti'' dai quali cerchiamo di liberarci con, appunto, il
\emph{divertimento} e lo \emph{svago}. Come possiamo includere il pensiero come
\emph{divertimento} dalle tediosit\`a della vita?

L'educazione di massa non aiuta: essa dona ai giovani una montagna di
strumenti d'indagine e di pensiero, ma nel fare questo non insegna mai a
divertirsi con essi -- a sfruttarli come fonte di divertimento. Potremmo
svolgere un'indagine, chiedendo agli studenti una risposta secca se discipline
(e gi\`a il nome le condanna) come la matematica, la filosofia, la
linguistica, la fisica, la storia dell'arte siano divertenti o meno.
Credo che la risposta sarebbe scontata. 

Anche la societ\`a non aiuta. C'\`e un sottile stigma che permea la percezione
diffusa di queste \emph{ginnastiche del pensiero}: si tratta di cose
\emph{distanti}, \emph{cervellotiche}, persino \emph{inutili} perch\'e col pensiero
\emph{certamente non si mangia} (valutazione quanto mai errata).
La politica alimenta questa percezione con populismi di tutte le risme.
Essa non ha alcun interesse a stimolare una ginnastica del pensiero:
un pensiero ben strutturato \`e anche un pensiero critico e difficile da
affrontare con argomenti populistici che funzionano bene, appunto, solo su
masse imbottite di \emph{diversioni} superficiali: il \emph{turismo} e la
\emph{fitness} sono utili e funzionali, il \emph{pensiero} no.

Si potrebbe obiettare che le nostre societ\`a stimolino il pensiero attraverso
la cultura, la quale \`e variamente sostenuta e finanziata dagli stati
evoluti. In effetti, la cultura presa come fenomeno globale \`e
indubbiamente una ginnastica del pensiero a tutti gli effetti -- e come
tale viene gi\`a fortemente penalizzata in termini numerici di pubblico.
Le mostre d'arte, i luoghi storici, i concerti godono di un successo tanto
maggiore quanto essi siano agganciabili o meno a flussi turistici -- questa
cultura \`e quindi pi\`u un indotto del turismo che la scelta di una
ricerca senziente da parte degli individui. Si prende ci\`o che passa il
convento, e la probabilit\`a che si tratti di meraviglie e capolavori
\emph{del passato} \`e talmente alta da costituire praticamente una certezza.

Forse, per meglio intenderci, potremmo equiparare questo tipo di cultura
alla \emph{ginnastica dolce} oppure al turismo delle \emph{scampagnate fuori porta}.
Una qualche ginnastica cerebrale avviene ed \`e indubbiamente importante e
fondamentale per poter affrontare ulteriori sforzi. Ma le occasioni per
incontrare queste \emph{ulteriori fatiche} del pensiero non si presentano mai.

Di nuovo, le nostre societ\`a e i nostri sistemi educativi non ci abituano a
pensare che la cultura sia qualcosa che si faccia, che si viva, alla quale si
partecipi nel \emph{hic et nunc} della contemporaneit\`a. La cultura \`e
sepolta nei libri, nelle partiture del passato, nelle stagioni secolari delle
arti visive -- ed siamo gi\`a tanto fortunati se abbiamo il privilegio di
allenare il nostro cervello sulle palestre di pensieri e opere che datano di 2, 3, 4
secoli fa per non dire oltre. Eppure, la \emph{sola} vera cultura \`e quella contemporanea --
tutto il resto fa parte del corredo strumentale che serve per analizzare e
comprendere la creazione odierna. Questo fatto \`e talmente disconosciuto
dalle societ\`a di oggi che persino cosidetti intellettuali paludati di una
qualche carica accademica pi\`u o meno importante si cimentano
nell'argomentare fatidiche \emph{morti dell'arte}, \emph{dissolvimenti della cultura} e
via discorrendo. Quella che, a pensarci bene, \`e un'ovviet\`a quasi banale
(la cultura \`e quella che si fa \emph{qui e ora})
sembra il concetto di digestione pi\`u ostica alle gi\`a ristrettissime masse
che sono pur attratte da un sostanzioso \emph{divertimento intellettuale}.

In questo interstizio si \`e oltretutto inserito il mercato. Certo, il mercato
della cultura \`e sempre esistito, ma nella massificazione delle societ\`a
questo mercato ha dovuto affrontare una sfida nella quale l'oggetto stesso (la
cultura appunto) \`e finita per scomparire. \emph{Prodotti culturali}
riconosciuti oggi da un vasto pubblico sono -- tra gli altri -- il cinema e la
musica di consumo. \`E ovvio che la necessit\`a di questi prodotti di dover
incontrare i gusti di un pubblico sempre pi\`u ampio per poter rientrare degli esorbitanti costi
di realizzazione -- o semplicemente per poter massimizzare i profitti -- non
consentono la crescita di una reale elaborazione culturale. Non \`e un caso
che il cinema non riesca a districarsi dalle maglie di un narrativismo che in
letteratura \`e gi\`a stato superato un secolo fa mentre la musica di consumo
faccia riferimento a forme e grammatiche ormai risalenti a secoli fa. E il segno
pi\`u eclatante che si tratti di \emph{culture surrogate} \`e dato dal
fatto che non evolvono: qualsiasi prodotto di qualsiasi periodo storico \`e
storicamente invertibile con un altro -- ci accorgiamo delle inversioni
soltanto per innovazioni tecniche e tecnologiche onnipresenti, non certo per i contenuti.
Infine, se accettassimo l'idea che i prodotti del mercato dell'intrattenimento
fanno parte del nostro \emph{fare cultura} contemporaneo, dovremmo accettare anche il 
fatto che il destino di quest'ultimo venga deciso nei consigli
d'amministrazione delle case cinematografiche o delle case discografiche -- a
valle di accurate indagini di mercato per individuare i gusti del
pubblico, come se questi non fossero influenzabili e influenzati dalle scelte dei primi in
un interminabile circolo vizioso.

La storia della cultura ci insegna che non \`e cos\`i che si \emph{fa cultura}.
La cultura si costruisce innanzitutto individualmente e poi collettivamente e
nasce dal confronto attivo di tutti i partecipanti a questo meraviglioso
processo. E la storia delle interazioni tra artisti e committenza e pubblico
\`e una storia troppo raramente raccontata -- anche perch\'e per molti versi
oscura e taciuta per la rara punteggiatura di motivi poco nobili. Cos\`i
facendo, non si alimenta una coscienza estesa del fatto che il \emph{fare
cultura} parte innanzitutto da noi stessi.

Come altro spiegare un tale distacco? Il fatto \`e che \emph{fare cultura} oggi \`e
un'attivit\`a estremamente faticosa e molto poco gratificante. Si combatte
contro tutto e contro tutti, dall'ambiguit\`a e fragilit\`a dei linguaggi contemporanei
sino alla distanza di un pubblico ignorante e affatto abituato non diciamo
alla contemporaneit\`a, ma nemmeno agli exploits culturali di cinquanta o
cento anni fa. In pi\`u, non si pu\`o consumare il \emph{fare cultura} --
bisogna farla, ovunque e con qualsiasi mezzo, e basta. Non si pu\`o quindi
costruirci sopra un mercato ed \`e persino difficile trovare gli specialisti
in grado di costruirci un piano di comunicazione adeguato -- condizione
essenziale per raggiungere qualsiasi tipo di pubblico si desideri conquistare.

Eppure, saremmo tentati di dichiarare che il \emph{fare cultura} oggi \`e il modo
pi\`u diretto ed efficace di procurarsi quel \emph{divertimento} profondo e
intenso in grado di riempire la propria esistenza di senso -- e nei casi pi\`u
brillanti e fortunati riempire anche quelle delle generazioni future.

Non dobbiamo quindi lasciare alcunch\'e d'intentato nel cercare di
diffondere l'idea che, come la cura del corpo attraverso l'esercizio fisico
produce effetti benefici sulla propria vita, anche l'esercizio intellettuale,
la ginnastica cerebrale possano diventare altamente strumentali a un benessere
collettivo che si traduca in un'accresciuta qualit\`a della vita.

\section{Divertimento e neuroscienze}

Una contemporaneit\`a molto pi\`u accettata, anche se a causa dell'estrema
specializzazione viene conosciuta solo superficialmente, \`e quella della
ricerca scientifica.

L'evoluzione rapidissima delle neuroscienze ha consentito di indagare anche in
ambiti preclusi alla ricerca sino a pochi anni fa. Concetti mal definiti
come \emph{sfera emotiva}, \emph{aspetti motivazionali}, \emph{senso di gratificazione}
sono ora ampiamente dibattuti in contesti sperimentali e scientifici -- e
un'opera di divulgazione ha gi\`a preso abbondantemente piede (prova ne sia
la copiosit\`a di presentazioni video a riguardo\footnote
{
  Una parzialissima videografia di video interessanti pu\`o essere la
  seguente: \url{https://youtu.be/n706_qp20Mk},
  \url{https://youtu.be/aqXmOb_fuN4}, \url{https://youtu.be/Mnd2-al4LCU},
  \url{https://youtu.be/sj6R1Tcjsl8}.
}).

Gli scienziati sembrano concordi nell'affermare il ruolo protagonista della
\emph{dopamina}, una sostanza chimica che funziona sia come ormone
che come neurotrasmettitore e viene prodotta in varie aree del cervello.
Il ruolo evolutivo della dopamina \`e quello di attivare la memoria di
esperienze gratificanti al fine di una loro eventuale replica, e forse ancora
di pi\`u di effettuare \emph{predizioni di gratificazione}, motivandoci ad
azioni collegate al raggiungimento degli obiettivi. Gli studi si concentrano
sulla dopamina perch\'e si \`e scoperto che le persone affette da dipendenze
hanno una produzione ridotta di dopamina -- lasciando intuire che le
dipendenze siano causate da una rimozione fisica dei controlli inibitori
prodotti nel cervello dall'interazione costante della dopamina con ormoni
collegati quali la \emph{serotonina} e l'\emph{ossitocina}.

Cosa c'entra tutto questo col divertimento? Sembra che il divertimento -- a qualsiasi
livello esso sia -- alimenti l'interazione di queste sostanze nel nostro
cervello generando i sensi di gratificazione e motivazione che associamo a
esso. Il punto \`e che certi divertimenti spingono verso la dipendenza
(inibendo progressivamente la produzione di dopamina) mentre altri mantengono
un equilibro ecosistemico nella produzione di ormoni cosiddetti ``del domani''
(quale appunto la dopamina) con quella degli ormoni del ``qui e ora'' (quali
la serotonina e l'ossitocina).

Le ricerche si sono concentrate sinora sulle tossicodipendenze, com'\`e giusto
che fosse -- producendo una copiosa messe di evidenze scientifiche e di
risultati pratici.
Forse si sta avvicinando il momento in cui potremo indagare
approfonditamente gli aspetti legati al divertimento. 

\end{document}
