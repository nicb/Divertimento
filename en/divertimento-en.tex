\documentclass{scrartcl}
\usepackage[english]{babel}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage[stable]{footmisc}
\usepackage{parskip}
\usepackage[mark]{gitinfo2}
\usepackage[leftbars]{changebar}

\newcommand{\versiontag}{ver.\gitAbbrevHash\ \ \gitAuthorDate}

\title{Divertimento\footnotemark[0]\\[-0.5\baselineskip]{\tiny (\versiontag)}}

\author{Nicola Bernardini}
\date{May 23, 2020}

\newcommand{\imagedir}{../images}

\setlength{\parskip}{1.2\baselineskip}
\setlength{\parindent}{0pt}

\begin{document}

\maketitle
\footnotetext{\hspace{1mm}
\includegraphics[width=0.06\textwidth]{\imagedir/cc-by-sa}
\emph{Divertimento} (2020) is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
Based on a work at \url{https://gitlab.com/nicb/Divertimento}.
}

\section{\emph{Divertimento} - An introduction}

\emph{[\emph{Divertimento} is an italian word that can roughly be translated in
english with the words ``fun'' or ``amusement''.
Etymology and ensuing connotations that are of interest
in this article get lost in translation so
I decided to keep the italian term.]}

% L'etimo del verbo \emph{divertire} \`e legato al latino
% \emph{divertere}. Altre parole significativamente collegate a questo verbo
% sono \emph{diverso} e \emph{divorzio}. Esso \`e composto dalla \emph{DI(S)}
% che significa allontanamento e \emph{VERTERE}, oss\`ia volgere.
% Quindi? \emph{volgere altrove}, far prendere altra direzione.

The etymology of the verb \emph{divertire} is related to the Latin \emph{divertere}.
Other words significantly connected to this term are \emph{diverse} and \emph{divorce}.
The term is composed of the particle \emph{DI(S)} which means
deviation or departure and \emph{VERTERE} which means to turn.
\emph{Divertire} stands thus for ``to turn around'', or ``to take another direction''.

% Il dizionario etimologico ci informa ovviamente anche del significato figurato
% di questa parola: "distogliere, [\ldots] ricreare, sollazzare, distraendo l'animo da
% cure e pensieri molesti".

The etymological dictionary also hints at the figurative meaning of this
word: ``to divert, [\ldots] recreate, amuse, distracting the soul from the harassing thoughts
of life''.

% L'idea che un \emph{divertimento} non sia semplice svago ma piuttosto un
% improvviso cambiamento di direzione, un dirottamento e infine una
% \emph{diversione} (appunto) non \`e mia: nelle lunghe chiaccherate con Luciano
% Berio era emerso questo significato pi\`u profondo\footnote{
% In queste discussioni si parlava dei molti \emph{Divertimenti} 
% da lui composti negli anni '50, spesso in compagnia e in complicit\`a
% con Bruno Maderna, in seno al loro lavoro di programmisti RAI.
% } che mi accompagna ancora oggi.
% Col passare del tempo ho capito che non solo il divertimento \`e un momento di apertura
% ad altro, all'inedito e inaspettato, ma che \`e anche una componente fondante
% del senso delle nostre vite -- il quale tende facilmente a perdersi nella
% stupida macina quotidiana dei problemi, dei doveri e delle aspettative di
% un'esistenza ormai irrimediabilmente alienata.

The idea that a ``divertimento'' is not mere entertainment but rather a sudden direction change
or a diversion is not mine:
such a deeper meaning emerged
in the long chats with Luciano Berio
\footnote{
Many of our discussions were centered around the several \emph{Divertimenti}
(which also happens to be a classical musical form)
he had written in the '50s, often together with Bruno Maderna, when he was
serving as a music program consultant within the Italian radio in Milan.
}
and it still with me today. Over time I realized that not only a \emph{divertimento}
is a moment of openness towards something else, maybe unprecedented and unexpected,
but it is also a grounding element of the meaning of our lives --
a meaning which is easily lost in the
stupid daily grinding of problems, duties and expectations of a
hopelessly alienated existence.

% Il problema \`e che non sappiamo pi\`u divertirci.

The problem is that we no longer know how to have fun, how to ``divertirci''
properly speaking.

% La nostra esistenza di individui \emph{per decreto} teorizzata da Zygmunt Bauman,
% oltre ad aver alienato il senso di elementi fondanti quali le passioni e il desiderio, 
% ci ha privato della capacit\`a (totalmente soggettiva e individuale)
% di \emph{divertirci}, ossia di volgere lo sguardo altrove,
% di raggiungere quel ``altro da s\'e'' che, solo, \`e in grado di restituire
% senso alle traiettorie di vita che, ironicamente, si allungano sempre di pi\`u
% - lasciando spazio a seconde e financo a terze vite.
% Se aggiungiamo in questa miscela l'evoluzione delle societ\`a odierne in
% ``societ\`a di servizi'' con conseguente emergenza di un ceto ``terziario
% avanzato'' al quale vengono regolarmente affidate mansioni burocratiche,
% ripetitive e per nulla gratificanti abbiamo un quadro pi\`u o meno completo (e
% terrificante) della situazione.
% La vita \`e diventata irrimediabilmente lunga e noiosa
% e il dilagare delle patologie depressive e delle dipendenze da agenti tossici
% come cibo, fumo, alcool e droghe di tutte le qualit\`a
% \`e un segnale esorbitante che facciamo fatica a ignorare.

Our existence of individuals by decree theorized by Zygmunt Bauman,
in addition to having alienated the sense of grounding elements such as passions and desire,
has deprived us of the ability (totally subjective and individual) to \emph{divertirci},
that is to turn our vision elsewhere, to reach that \emph{outer self}
which alone is capable of giving back meaning
to the trajectories of a life that, ironically, is getting longer and longer
-- leaving space to second and even to third lives. If we add the evolution of
today's societies in \emph{service societies} with the consequent uprising of an
\emph{advanced tertiary class} which is
regularly entrusted with bureaucratic, repetitive and other non--gratifying tasks
we have a more or less complete (and terrifying) picture of the situation.
Life has become hopelessly long and boring and the spread of depressive pathologies
and addictions to toxic agents such as food, smoking, alcohol and drugs of all
sorts is an exorbitant signal that is indeed hard to ignore.

% Naturalmente, tutte le societ\`a ``civili'' sono corse ai ripari.
% Anzi: \`e nata una vera e propria industria del divertimento con proventi da capogiro.
% Veniamo tutti imbottiti di svaghi di massa -- gli unici con i quali si possano
% massimizzare i profitti -- e la societ\`a dell'informazione si \`e
% perfettamente inserita in questo ecosistema rendendo onanistico (e quindi
% endogeno) il \emph{divertimento}. I \emph{social networks}, la televisione
% \emph{on demand}, la musica \emph{on tap}, le notizie in tempo reale -- sono
% tutti tasselli di un gigante mosaico che martella le nostre vite quotidiane
% per ``divertirci'' in ogni momento. La verit\`a \`e che si tratta di
% ``divertimenti'' posticci, fatui, privi di consistenza semantica -- sono in
% realt\`a \emph{distrazioni} pi\`u che divertimento. Ed ecco che compaiono gli
% studi sulle dipendenze da \emph{smartphone}, da videogiochi, da pornografia,
% ecc. E, di converso, ecco i libri su come ``essere migliori'', come ``essere
% felici'', come ``vivere intensamente la propria vita'', ecc.

Of course, all civil societies have taken measures.
In fact, a proper \emph{fun industry} with mind--boggling revenues was created.
We are all stuffed with mass leisures --
the only ones which allow maximization of profits --
and the information society did integrate perfectly
into this ecosystem transforming this fun into an onanistic (and
therefore endogenous) one.
Social networks, television on demand, music on tap, the news in real time --
these are all pieces of a giant mosaic hammering
on our daily lives for us to have fun at all times.
The truth is, these are
frivolous, ephemereal and semantic-free amusements -- they are actually distractions rather than fun.
No wonder there is a multiplication
of studies on smartphone,
video game, pornography, etc. addictions.
And no wonder an endless literature on \emph{how to be better},
\emph{how to be happy}, \emph{how to live one's life intensely}, etc.
has appeared.

% Il fatto \`e che il \emph{divertimento} \`e faticoso.
The fact is that having \emph{real} fun is a laborious process.

% Per divertirsi davvero, per avere delle epifanie di pensiero che facciano
% crescere la consapevolezza di mondo che ci circonda, occorre studiare.
% Pi\`u studiamo, pi\`u approfondiamo i nostri oggetti di desiderio, pi\`u
% elaboriamo un pensiero critico in grado di analizzarli, e pi\`u ci
% \emph{divertiamo} con essi - e questa volta si tratta di \emph{divertimenti}
% importanti, che riempiono di senso le nostre vite dandoci qualcosa al quale
% aggrapparci per non essere trascinati via dai flutti del quotidiano.

To really have fun, to have thought epiphanies that make our consciousness
of the world grow we need \emph{to study}.
The more we study, the deeper we observe our objects of desire,
the more we are able to elaborate a critical thought capable of analyzing them,
and the more we have fun with them -- and this time we are talking about \emph{important entertainment},
which will fill our lives with meaning and will give us something to hold on to
in order not to be dragged away by the billows of everyday life.

% In verit\`a, ci sono alcuni ``divertimenti di massa'' che godono di pubblici
% estremamente vasti nonostante siano assai faticosi.
% Vorrei delinearne qui due:
% la \emph{fitness} e il \emph{turismo}.
% Entrambi pretendono dai propri praticanti sacrifici e fatica,
% e forse per questo riescono a dare pi\`u senso di altri ``divertimenti''.
% Come funzionano questi divertimenti?

To tell the truth, there are some mass amusements that enjoy vast audiences
despite being very fatiguing. I would like to outline two of them here: \emph{fitness} and \emph{tourism}.
They both demand sacrifices and effort from their practitioners, and perhaps because of this they succeed
to make more sense out of life than other activities.
How do these amusements work?

% La \emph{fitness} produce dei risultati visibili sul benessere e sull'aspetto
% fisico in un tempo relativamente breve, e si tratta di risultati estremamente
% gratificati dalle societ\`a di oggi: l'aderenza pi\`u rigorosa ai canoni
% vigenti di bellezza fisica unita al generale stato di salute dell'individuo
% forniscono incentivi pi\`u che sufficienti ad affrontare sacrifici e fatica.
% Prova ne sia la quantit\`a di palestre che sono comparse a qualsiasi
% latitudine del globo: la \emph{fitness} sembra essere un linguaggio universale
% delle societ\`a contemporanee. Come in tutte le produzioni di consumo,
% l'offerta si \`e diversificata all'inverosimile, creando ``prodotti'' per
% tutti i palati, dalla ``ginnastica dolce'' al ``jogging'', dal \emph{pilates}
% al \emph{cross--fit} al \emph{body building}. E -- come giustamente rilevava
% Bauman -- mentre la salute \`e una qualit\`a binaria (\`e presente o
% mancante), non ci sono limiti alla ``quantit\`a'' di \emph{fitness}.
% Essa non \`e mai abbastanza: una vera gallina dalle uova d'oro per
% un mondo consumista come il nostro.

Fitness produces visible results both on well--being and on physical appearance in a  relatively short time,
and these results are well gratified in today's societies:
the strictest adherence to the current canons of physical beauty combined with the general state of health
of each individual provide consistent incentives to face sacrifices and effort.
The number of gym and fitness centers that have sprung out at any latitude on the globe
are certainly a good indicator of the fact that this activity is in large demand:
fitness seems to be a universal language of contemporary societies. As in all
consumer productions, the offer has diversified beyond imagination, creating products for
all palates, from \emph{soft gym} to \emph{jogging}, from \emph{pilates} to \emph{cross-fit} to \emph{body building}.
And -- as Bauman rightly pointed out -- while health is a binary quality (it is present
or missing), there is no limit to the amount of fitness. It is never enough:
it is the perfect silver bullet in this consumeristic world of ours.

% Il turismo \`e un divertimento costoso che ha spesso bisogno di sacrifici
% consistenti da parte degli appassionati. Senza dimenticare che viaggiare \`e
% un'attivit\`a faticosa e talvolta tediosa. , il turismo \`e
% praticato da folle oceaniche di persone che si spostano tutti gli anni
% in ogni parte del globo - lo fanno comunque, anche se per il resto della
% propria vita abitano in luoghi meravigliosi (a loro volta visitati da
% \emph{altre} folle oceaniche). Cos'\`e che alimenta questo ``divertimento
% faticoso''? I motivi sono evidenti quanto ovvi: la vita alienata di oggi non
% ci consente di accumulare avvenimenti sostanziali, eclatanti, che si
% sedimentino prima nel nostro immaginario e poi nella nostra memoria.
% Abbiamo tutti bisogno quindi di questi ``surrogati di vissuto'' che possano
% costituire una possibile ``semantica di vita'': trascorriamo la maggior parte
% di quest'ultima senza avere assolutamente nulla da raccontare di una
% quotidianit\`a devastante, ma i nostri ``giri turistici'' colmano la vacuit\`a
% di fotografie e di ricordi pi\`u o meno superficiali.
% Anche qui, ce n'\`e per tutti i gusti: dalle \emph{vacanze--avventura} alle
% mete esotiche, dalle citt\`a d'arte alle antichit\`a alla natura (pseudo--)selvaggia.
% Ce n'\`e per tutti i gusti, ed \`e virtualmente impossibile visitare/vivere
% tutto in una sola vita, per quanto questa si allunghi e il turismo diventi
% ``mordi--e--fuggi''. Non ci meraviglieremo quindi del successo di massa di
% questo ``divertimento''.

Tourism is expensive entertainment that often requires substantial sacrifices from
its enthusiasts, besides the fact that traveling itself can be a tiring and tedious activity.
Despite these facts, tourism is practiced every year by oceanic crowds who 
travel to every part of the globe -- and they do it anyway, even if
their daily lives are spent in wonderful locations (which may in turn get visited
by other elements of the same crowd).
What kind of reward is feeding this fatiguing form of amusement?
Its motivations are both evident and obvious:
today's alienated life does not allow us to accumulate strikingly substantial events,
those which may settle before in our imagination and afterwards in our memory.
We therefore all need these experience surrogates that can constitute a
possible meaning of life: we spend most of the latter without having
anything to tell about our devastating everyday grinding,
but our touristic trips fill this gap with photographs and more or less superficial memories.
There is something for everyone in this case too: from \emph{adventure holidays} to \emph{exotic destinations},
from \emph{art cities} to \emph{antiquity sites} to the \emph{wild} (pseudo--)\emph{nature}.
There is something for all tastes, and it is virtually
impossible to visit/live everything in one life, however long it is
and however tourism becomes a \emph{hit--and--run} one.
No, the mass success of such \emph{divertimento} cannot be a surprise for anybody.

\section{The gymnastics of thought}

% Cosa manca in maniera eclatante in questa minuscola disamina dei ``divertimenti faticosi''?

What is missing in this minuscule outline of hard \emph{divertimenti}?

Thought -- or mind, if you will.

% Pensare \`e ritenuto dai pi\`u un'attivit\`a non
% solo faticosa, ma anche i cui sforzi somigliano troppo a quelli delle ``cure e
% dei pensieri molesti'' dai quali cerchiamo di liberarci con, appunto, il
% ``divertimento'' e lo ``svago''. Come possiamo includere il pensiero come
% ``divertimento'' dalle tediosit\`a della vita?

Not only is thinking generally considered to be a tiring activity.
Its efforts are way too similar to those ``harassing thoughts of life''
which we're trying to escape with fun and entertainement.
How can we include thinking as a diversion from the tediousness of life?

% L'educazione di massa non aiuta: essa dona ai giovani una montagna di
% strumenti d'indagine e di pensiero, ma nel fare questo non insegna mai a
% divertirsi con essi -- a sfruttarli come fonte di divertimento. Potremmo
% svolgere un'indagine, chiedendo agli studenti una risposta secca se discipline
% (e gi\`a il nome le condanna) come la matematica, la filosofia, la
% linguistica, la fisica, la storia dell'arte siano ``divertenti'' o meno.
% Credo che la risposta sarebbe scontata. 

Mass education will not help: while it provides young people with a formidable
quantity of mind and investigation tools,
it hardly teaches at all to have fun with them
-- that is, to exploit them as a source of fun. 
We could run a quick investigation asking students for a straight answer
whether disciplines such as mathematics, philosophy,
linguistics, physics, art history are fun or not.
We should not be surprised of the results.

% Anche la societ\`a non aiuta. C'\`e un sottile stigma che permea la percezione
% diffusa di queste ``ginnastiche del pensiero'': si tratta di cose
% ``distanti'', ``cervellotiche'', persino ``inutili'' perch\'e col pensiero
% ``certamente non si mangia'' (valutazione quanto mai errata).
% La politica alimenta questa percezione con populismi di tutte le risme.
% Essa non ha alcun interesse a stimolare una ``ginnastica del pensiero'':
% un pensiero ben strutturato \`e anche un pensiero critico e difficile da
% affrontare con argomenti populistici che funzionano bene, appunto, solo su
% masse imbottite di ``diversioni'' superficiali: il \emph{turismo} e la
% \emph{fitness} sono utili e funzionali, il \emph{pensiero} no.

Society doesn't help either.
There is a subtle stigma that permeates the widespread perception of
these \emph{gymnastics of the mind}: these are \emph{distant}, \emph{nerdish},
even \emph{useless}
because you can hardly eat with thought (a very wrong evaluation altogether).
Politics reinforces this perception with populisms of all kinds. It certainly has no
interest in stimulating some gymnastics of thought: a well structured thought is also
critical thinking which will not accept populistic arguments. These work well,
in fact, only on masses stuffed with superficial diversions. Thus,
while tourism and fitness are useful and functional to distract ourselves,
thinking is definitely not taken into consideration.

% Si potrebbe obiettare che le nostre societ\`a stimolino il pensiero attraverso
% la cultura, la quale \`e variamente sostenuta e finanziata dagli stati
% evoluti. In effetti, la ``cultura'' presa come fenomeno globale \`e
% indubbiamente una ``ginnastica di pensiero'' a tutti gli effetti -- e come
% tale viene gi\`a fortemente penalizzata in termini numerici di pubblico.
% Le mostre d'arte, i luoghi storici, i concerti godono di un successo tanto
% maggiore quanto essi siano agganciabili o meno a flussi turistici -- questa
% ``cultura'' \`e quindi un indotto del turismo pi\`u che una scelta di una
% ricerca senziente da parte degli individui. Si prende ci\`o che passa il
% convento, e la probabilit\`a che si tratti di meraviglie e capolavori
% \emph{del passato} \`e talmente alta da costituire praticamente una certezza.

It could be argued that our societies stimulate thought through culture.
Yes, culture is variously supported and financed by advanced states.
True, culture taken as a global phenomenon is undoubtedly a gym of thought in all due respects.
As such, its audience is already heavily penalized in numerical terms.
Art exhibitions, historical sites and concerts enjoy success when they are
related to tourist flows. This kind of culture is therefore more a form of induced tourism
than the conscious choice of investigating individuals.
You take whatever happens to be available,
and the probability that what is actually available is a collection
of wonders and masterpieces of the past is so high
to practically become a certainty.

% Forse, per meglio intenderci, potremmo equiparare questo tipo di ``cultura''
% alla ``ginnastica dolce'' ovvero al turismo delle ``scampagnate fuori porta''.
% Una qualche ginnastica cerebrale avviene ed \`e indubbiamente importante e
% fondamentale per poter affrontare ulteriori sforzi. Ma le occasioni per
% incontrare queste ``ulteriori fatiche'' del pensiero non si presentano mai.

We could equate this type of \emph{culture} to \emph{soft gymnastics}
or to occasional \emph{country house tourism}. Some brain gymnastics
occur and they are undoubtedly helpful to be able to face further efforts.
But \emph{other} opportunities to meet these further fatigues of thought are
never taken into consideration.

% Di nuovo, le nostre societ\`a e i nostri sistemi educativi non ci abituano a
% pensare che la ``cultura'' sia qualcosa che si fa, che si vive, alla quale si
% partecipa nel \emph{hic et nunc} della contemporaneit\`a. La cultura \`e
% sepolta nei libri, nelle partiture del passato, nelle stagioni secolari delle
% arti visive -- ed siamo gi\`a tanto fortunati se abbiamo il privilegio di
% allenare il nostro cervello sulle palestre di pensieri e opere che datano di 2, 3, 4
% secoli fa per non dire oltre. Eppure, questa \emph{\`e} la cultura --
% tutto il resto fa parte del corredo strumentale che serve per analizzare e
% comprendere la creazione odierna. Questo fatto \`e talmente disconosciuto
% dalle societ\`a di oggi che persino cosidetti intellettuali paludati di una
% qualche carica accademica pi\`u o meno importante si cimentano
% nell'argomentare fatidiche ``morti dell'arte'', ``sparizioni della cultura'' e
% via discorrendo. Quella che, a pensarci bene, \`e un'ovviet\`a quasi banale
% sembra il concetto di digestione pi\`u ostica alle gi\`a ristrettissime masse
% che sono pur attratte da un sostanzioso ``divertimento intellettuale''.

Once again, our societies and our education systems do not help us in discovering
that culture is something that gets done, lived, participated by everybody in the \emph{hic et nunc}
of contemporaneity. We are convinced that culture is buried in books, in past scores, in
age-old visual arts - and we should consider ourselves indeed very lucky
when we have the privilege
to train our brains on the gyms of thoughts and works dating from 2, 3, 4
centuries ago -- not to say any longer than that.
Still, the only real culture is \emph{contemporary culture} --
all the rest should be part of the instrumental kit used to analyze and understand today's creations and inventions.
This fact is so neglected by contemporary societies that many highly--regarded
intellectuals with a more or less important academic pedigree
try their hand at arguing fateful \emph{deaths of art}, \emph{dissolution of culture} and so on.
Thus, what is almost trivial in its obviousness
(the fact that culture is what gets done here and now)
is a very hard concept to digest even by
the already very restricted masses which are attracted
by substantial intellectual entertainement.

% In questo interstizio si \`e oltretutto inserito il mercato. Certo, il mercato
% della cultura \`e sempre esistito, ma nella massificazione delle societ\`a
% questo mercato ha dovuto affrontare una sfida nella quale l'oggetto stesso (la
% cultura appunto) \`e finita per scomparire. I ``prodotti culturali''
% riconosciuti oggi da un vasto pubblico sono -- per esempio -- il cinema e la
% musica di consumo. \`E ovvio che la necessit\`a di questi prodotti di dover
% incontrare i gusti di un pubblico sempre pi\`u per poter rientrare dei costi esorbitanti
% di realizzazione -- o semplicemente per poter massimizzare i profitti -- non
% consentono la crescita di una reale elaborazione culturale. Non \`e un caso
% che il cinema non riesca a districarsi dalle maglie di un narrativismo che in
% letteratura \`e gi\`a stato superato un secolo fa mentre la musica di consumo
% fa riferimento a forme e grammatiche ormai risalenti a secoli fa. E il segno
% pi\`u eclatante che si tratta di ``culture'' di second'ordine \`e dato dal
% fatto che non evolvono: qualsiasi prodotto di qualsiasi periodo storico \`e
% storicamente invertibile con un altro -- ci accorgiamo delle inversioni
% soltanto per le innovazioni tecniche e tecnologiche, non certo per i contenuti.
% Infine, se accettassimo l'idea che i prodotti del mercato dell'intrattenimento
% fanno parte del nostro ``fare cultura'' contemporaneo, dovremmo concordare sul
% fatto che il destino di quest'ultimo venga deciso nei consigli
% d'amministrazione delle case cinematografiche o delle case discografiche -- a
% valle di accurate indagini di mercato per individuare i ``gusti del
% pubblico'', come se questi non fossero influenzati dalle scelte dei primi in
% un interminabile circolo vizioso.

Furthermore, the market has tried a (so far very successeful) attempt at
filling this gap.
Of course, the culture market has existed for centuries,
but in the massification process this market had to face
a challenge in which the object itself (precisely culture) ended up disappearing.
\emph{Cultural} products which are acknowledged and taken over today
by large audiences are -- among others -- cinema
and commercial music.
It is obvious that such products need to meet
the tastes of very wide audiences
to be able to strive which their exorbitant costs of
realization -- or else to simply maximize profits.
This need does not allow the growth of a real cultural elaboration.
It is certainly no coincidence that cinema fails
to extricate itself from the maze of a narrativism that has already been overcome in literature
at least a century ago while commercial music refers to forms and grammars now
whose references can be found centuries ago.
The most striking evidence that these are surrogate cultures is given by
fact that they do not evolve: any outcome of these \emph{cultures} of any historical period
can be substituted by another from a different era --
the only way put all products in a chronological order is by analyzing
their production technologies: contents alone would not give us a
clue\footnote{
In fact, in cinema this is exactly what \emph{remakes} are about.
}.
Finally, if we accepted the idea that
entertainment market products are part of our contemporarary culture,
we should also admit that the fate of the latter is decided
by the boards of directors of film or record companies -- 
perhaps through accurate market researchs to identify the \emph{tastes of the public}
as if these were not influenced by the choices of the formers in an endless vicious circle.

% La storia della cultura ci insegna che non \`e cos\`i che si ``fa cultura''.
% La cultura si costruisce innanzitutto individualmente e poi collettivamente e
% nasce dal confronto attivo di tutti i partecipanti a questo meraviglioso
% processo. E la storia delle interazioni tra artisti e committenza e pubblico
% \`e una storia troppo raramente raccontata -- anche perch\'e per molti versi
% oscura e taciuta per la rara punteggiatura di motivi poco nobili. Cos\`i
% facendo, non si alimenta una coscienza estesa del fatto che il ``fare
% cultura'' parte innanzitutto da noi stessi.

The history of culture teaches us that this is not the way \emph{culture gets created}.
Culture is first built individually, then collectively and finally it arises
from the active confrontation of all participants in this wonderful process.
The history of the interactions between artists, their patrons and their audience
gets too rarely told and is quite obscure in many ways.
Because of that, we may not be able to promote an extended consciousness of the fact
that \emph{culture making} is a process that starts from ourselves in the first place.

% Come altro spiegare un tale distacco? Il fatto \`e che ``fare cultura'' oggi \`e
% un'attivit\`a estremamente faticosa e molto poco gratificante. Si combatte
% contro tutto e contro tutti, dall'ambiguit\`a e fragilit\`a dei linguaggi contemporanei
% sino alla distanza di un pubblico ignorante e affatto abituato non diciamo
% alla contemporaneit\`a, ma nemmeno agli exploits culturali di cinquanta o
% cento anni fa. In pi\`u, non si pu\`o consumare il ``fare cultura'' --
% bisogna farla, ovunque e con qualsiasi mezzo, e basta. Non si pu\`o quindi
% costruirci sopra un mercato ed \`e persino difficile trovare gli specialisti
% in grado di costruirci un piano di comunicazione adeguato -- condizione
% essenziale per raggiungere qualsiasi tipo di pubblico si desideri conquistare.

How else can we explain such a dramatic distance?
It is a fact that \emph{culture making} is
an activity that is extremely hard and quite unrewarding today.
We fight against everything and everyone,
from the ambiguity and fragility of contemporary languages
to the distance of ignorant audiences which are hardly accustomed
to the language exploits of cultural events of fifty or a hundred years ago,
let alone now.
In addition, contemporary culture cannot be consumed --
you have to do it, everywhere, by any means, and that's it.
Therefore a market cannot be build over it
and it's even difficult to find specialists
who can create adequate communication plans --
an essential condition to reach any type audience today.

% Eppure, saremmo tentati di dichiarare che il ``fare cultura'' oggi \`e il modo
% pi\`u diretto ed efficace di procurarsi quel ``divertimento'' profondo e
% intenso in grado di riempire la propria esistenza di senso -- e nei casi pi\`u
% brillanti e fortunati riempire anche quelle delle generazioni future.

Still, we are tempted to declare that \emph{culture making} is today the most direct and effective way
to get that deep and intense \emph{divertimento} that can fill up our existence with meaning --
and in most brilliant and fortunate cases it will fill even those of future generations.

% Non vale quindi la pena di lasciare alcunch\'e d'intentato nel cercare di
% diffondere l'idea che, come la cura del corpo attraverso l'esercizio fisico
% produce effetti benefici sulla propria vita, anche l'esercizio intellettuale,
% la ginnastica cerebrale possano diventare altamente strumentali a un benessere
% collettivo che si traduca in un'accresciuta qualit\`a della vita?

We should therefore not leave any stone unturned in trying to spread the idea that,
in the very same way body care through exercise produces beneficial effects on our life,
intellectual exercise and brain gymnastics can become highly instrumental too
to a collective well--being that translates into an increased quality of life.

\section{\emph{Divertimento} and neurosciences}

% Una contemporaneit\`a molto pi\`u accettata, anche se a causa dell'estrema
% specializzazione viene conosciuta solo superficialmente, \`e quella della
% ricerca scientifica.

Scientific research enjoys a much better acceptance and prestige,
even though it is grasped only very superficially due to the extreme degree of
specialization of all domains.

% L'evoluzione rapidissima delle neuroscienze ha consentito di indagare anche in
% ambiti preclusi alla ricerca sino a pochi anni fa. Concetti mal definibili
% come ``sfera emotiva'', ``aspetti motivazionali'', ``senso di gratificazione''
% sono ora ampiamente dibattuti in contesti sperimentali e scientifici -- e
% un'opera di divulgazione ha gi\`a preso abbondantemente piede (prova ne sia
% la copiosit\`a di presentazioni video a riguardo\footnote
% {
%   Una parzialissima videografia dei pi\`u interessanti pu\`o essere la
%   seguente: \url{https://youtu.be/n706_qp20Mk},
%   \url{https://youtu.be/aqXmOb_fuN4}, \url{https://youtu.be/Mnd2-al4LCU},
%   \url{https://youtu.be/sj6R1Tcjsl8}.
% }.

The rapid evolution of neurosciences has allowed scientists
to investigate even in areas unavailable to research until a few years ago.
Ill--defined concepts as \emph{emotional context},
\emph{motivational aspects}, \emph{sense of gratification} and the like
are now widely debated in experimental and scientific contexts
-- and dissemination work is already well on its way
(evidence of this work is in the abundance of video presentations around these
topics\footnote{%
  An extremely limited and partial videography of interesting talks could be:
  \url{https://youtu.be/n706_qp20Mk},
  \url{https://youtu.be/aqXmOb_fuN4}, \url{https://youtu.be/Mnd2-al4LCU},
  \url{https://youtu.be/sj6R1Tcjsl8}.
}).

% Gli scienziati sembrano concordi nell'affermare il ruolo protagonista della
% \emph{dopamina}, una sostanza chimica che funziona sia come ormone
% che come neurotrasmettitore e viene prodotta in varie aree del cervello.
% Il ruolo evolutivo della dopamina \`e quello di attivare la memoria di
% esperienze gratificanti al fine di una loro eventuale replica, e forse ancora
% di pi\`u di effettuare \emph{predizioni di gratificazione}, motivandoci ad
% azioni collegate al raggiungimento degli obiettivi. Gli studi si concentrano
% sulla dopamina perch\'e si \`e scoperto che le persone affette da dipendenza
% hanno una produzione ridotta di dopamina ridotta -- lasciando intuire che le
% dipendenze siano causate da una rimozione fisica dei controlli inibitori
% prodotti nel cervello dall'interazione costante della dopamina con ormoni
% collegati quali la \emph{serotonina} e l'\emph{ossitocina}.

Scientists seem to agree in asserting the leading role of \emph{dopamine},
a chemical substance that works both as a hormone and as a neurotransmitter
and is produced in several areas of the brain.
The evolutionary role of dopamine is to activate
memory of rewarding experiences in view of their possible replication,
and perhaps even more to be able to create \emph{gratification predictions},
motivating us to actions related to the
achievement of rewarding objectives.
Studies focus on dopamine because it turned out that
people with important addictions have a reduced dopamine production -
suggesting that addictions are caused by a physical removal of inhibitory controls
produced by the brain through the constant interaction of dopamine with connected hormones
such as \emph{serotonin} and \emph{oxytocin}.

% Cosa c'entra tutto questo col divertimento? Sembra che il divertimento -- a qualsiasi
% livello esso sia -- alimenta l'interazione di queste sostanze nel nostro
% cervello generando i sensi di gratificazione e motivazione che associamo a
% esso. Il punto \`e che certi ``divertimenti'' spingono verso la dipendenza
% (inibendo progressivamente la produzione di dopamina) mentre altri mantengono
% un equilibro ecosistemico nella produzione di ormoni cosiddetti ``del domani''
% (quale appunto la dopamina) con quella degli ormoni del ``qui e ora'' (quali
% la serotonina e l'ossitocina).

What does all this have to do with fun and \emph{divertimento}? It seems that fun --
at any level -- feeds the interaction of these substances in our brain by generating the
senses of gratification and motivation that we associate with it.
The point is that certain amusements
push you towards addiction (progressively inhibiting dopamine production)
while others maintain an ecosystem balance in the production of the so--called hormones
``of tomorrow'' (such as dopamine, which acts as a predictor) with the ``here and now'' hormones (such as
serotonin and oxytocin).

% Le ricerche si sono concentrate sinora sulle tossicodipendenze, com'\`e giusto
% che fosse -- producendo una copiosa messe di evidenze scientifiche e di
% risultati pratici.
% Forse si sta avvicinando il momento in cui potremo indagare
% approfonditamente gli aspetti legati al divertimento. 

Research has focused so far on drug addictions, and rightly so of course --
producing a copious harvest of scientific evidence and practical results.
However, we might be approaching the moment in which we will be able to investigate
in depth the aspects related to \emph{divertimento}.

\end{document}
