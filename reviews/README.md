# Critical reviews and responses to *Divertimento*

This folder contains some of the reactions that the *Divertimento* pamphlet
got. Many reactions were scattered along several communication channels (mail,
chat lines, telephone), using two different languages (italian and english),
making it difficult to keep the integrity of all the interaction.

However, some were particularly relevant as they spurred other fundamental
questions which urged a reply on my side. I will do my best to keep all
materials concerning these comments in the subfolders contained herein.

I wish to thank all of those who took the time to read the pamphlet in the
first place, and even more so those who had the wish to express their points
of view on it. I am especially grateful of critical remarks, because they
allowed me to look at it from other perspectives - which is a rare privilege
nowadays.

## Additional notes

A few months after having written it and sent it around, I can certainly see
one very critical aspect which I should probably amend in a "second edition"
of this pamphlet (should I ever decide to write it): in order to make its
point it draws on way too many issues and problems which are hardly connected
together - thus making it difficult to focus on the main point (*we no longer
know how to "divertirci"*). The pamphlet is way too distracting on ancillary
arguments (such as *is tourism a good source of fun or not*? or *is film
making contributing to culture or not*? and many more) which are hugely debatable
in themselves. As a result, I got all sorts of remarks on these ancillary
arguments rather than strong opinions against or in favor of the main problem
I wanted to address (*divertimento is not a mind-numbing task as it is sold
today, it is a hard task that fulfills one's own existence*). Of course, this
is not the readers'/reviewers' fault, but entirely my responsibility: in this
version, the pamphlet is simply very poorly written (to put it mildly).
I am not sure whether it can actually do without some efficient practical
examples, but these certainly should not take too much momentum out of the
main topic as they do now.
