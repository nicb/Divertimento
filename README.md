# Divertimento

These are the *LaTeX* sources of a pamphlet on the concept of *Divertimento*, which is the italian word for *Amusement* or *Fun*.

[Here](./reviews/README.md) you may find some reviews and discussions
which resulted out of it.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Divertimento</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/nicb/Divertimento" rel="dct:source">https://gitlab.com/nicb/Divertimento</a>.
[Full text license](./LICENSE)
